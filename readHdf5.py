#!/usr/bin/python
# -*- coding: utf-8 -*-
import h5py
import numpy as np
import re
from xml.dom import minidom

NX_CLASS = 'NX_CLASS'
NX_INSTRUMENT = 'NXinstrument'
NX_DETECTOR = 'NXdetector'
NX_DATA = 'NXdata'
NX_ENTRY = 'NXentry'


def numberImageOfData(data):
    shapeData = np.shape(data)
    size = len(shapeData)
    nbImages = 1

    for i in range(size - 2):
        nbImages = nbImages * shapeData[i]

    return nbImages


def sizeImage(data):
    shapeData = np.shape(data)
    size = len(shapeData)
    nbImages = 1

    for i in range(size - 2, size):
        nbImages = nbImages * shapeData[i]

    return nbImages

def getShapeData(fileName,pathToData):
    try:
        file_handle = h5py.File(fileName, 'r')
        dataset = file_handle[pathToData]
        shapeData = np.shape(dataset)
        size = len(shapeData)

    except IOError as e:
        print('cannot open ' + fileName + ':\n' + str(e))

    return shapeData[size-2:]

def readFile(fileName, pathToData,startIndiceImage,endIndiceImage, ndImages):
    try:
        file_handle = h5py.File(fileName, 'r')
        dataset = file_handle[pathToData]
        nbImages = numberImageOfData(dataset)
        shapeData = np.shape(dataset)
        size = len(shapeData)
        sizeImage = shapeData[size- 2] * shapeData[size - 1]
        reshapeImages = np.reshape(dataset, (nbImages * sizeImage))
        newDims = np.shape(reshapeImages)
        ndImages[:] = reshapeImages[startIndiceImage*sizeImage:(endIndiceImage+1)*sizeImage]
    except IOError as e:
        print('cannot open ' + fileName + ':\n' + str(e))

    return np.shape(ndImages)



def getStaticPath(fileName, dictPath, dataKey):
    dataExist = checkDataInDictionnary(dictPath, dataKey)
    staticPath = ''
    if dataExist:
        pathKey = getPathOfDataKey(dictPath, dataKey)
        staticPath = getPathToImage(fileName, pathKey)

    return staticPath[0]


def loadMask(maskPath, images):
    file = open(maskPath, mode='r', encoding='utf-8-sig')
    lines = file.readlines()
    file.close()
    my_dict = {}
    my_list = []
    for line in lines:
        line = line.split(';')
        line = [i.strip() for i in line]
        for j in range(len(line)):
            my_list.append(line[j])

    return my_list


def checkDataInDictionnary(dictonnaryPath, dataKey):
    xmldoc = minidom.parse(dictonnaryPath)
    itemlist = xmldoc.getElementsByTagName('item')
    found = False
    for s in itemlist:
        if s.attributes['key'].value == dataKey:
            found = True
            print('i found datakey ' + dataKey + ' in dictionnary ')
    return found


def getPathOfDataKey(dictionnaryPath, datakey):
    xmldoc = minidom.parse(dictionnaryPath)
    itemlist = xmldoc.getElementsByTagName('item')
    path = ''
    for s in itemlist:
        if s.attributes['key'].value == datakey:
            childNodes = s.childNodes
            for child in childNodes:
                if child.nodeType == minidom.Node.ELEMENT_NODE:
                    if child.nodeName == 'path':
                        value = child.firstChild.nodeValue
                        path = value

    return path


def applyMask(vectorImages, vectorMask):
    sizeMask = len(vectorMask)
    for i in range(len(vectorImages)):
        if vectorMask[i % sizeMask] == 0:
            vectorImages[i] = float('nan')

    return vectorImages


def splitPath(path, separator):
    return path.split(separator)


def getPathToImage(fileName, pathToDataSet):
    path = pathToDataSet
    listPath = path.split('/')
    checkNxclass = False
    onlyNxsClass = False
    findRgx = False
    foundNxEntry = False
    checkAttr = False
    nameAttrToCheck = ''
    valueAttrToCheck = ''
    numberRgx = 0
    nameClass = ''
    listFinalPath = []
    tmpPath = ''
    lvlPath = 0
    maxLvlPath = len(listPath)
    nxEntry = ''

    try:
        file_handle = h5py.File(fileName, 'r')
        for key in list(file_handle.keys()):
            if foundNxEntry == False:
                if file_handle[key].attrs['NX_class'].decode('utf-8') \
                        == 'NXentry':
                    nxEntry = '/' + key
                    tmpPath = nxEntry
                    foundNxEntry = True
                    break
    except IOError as e:
        print('cannot open ' + fileName + ':\n' + str(e))

    for subPath in listPath[1:]:
        lvlPath = lvlPath + 1
        checkNxclass = False
        findRgx = False
        checkAttr = False

        if subPath.find('{') != -1:
            checkNxclass = True
            indiceStartClass = subPath.find('{')
            indiceEndClass = subPath.find('}')
            nameClass = subPath[indiceStartClass + 1:indiceEndClass]
            subPath = subPath[0:indiceStartClass]
            findRgx = subPath.find('*') != -1 or subPath != ''
            if subPath == '':
                onlyNxsClass = True
        elif subPath.find('@') != -1:

            # # find attribute case

            checkAttr = True
            indiceAttr = subPath.find('@')
            indiceValueAttr = subPath.find('=')
            nameAttrToCheck = subPath[indiceAttr + 1:indiceValueAttr]
            valueAttrToCheck = subPath[indiceValueAttr + 1:]
            subPath = subPath[0:indiceAttr]
        elif subPath.find('*') != -1:

            # # find rgx case

            findRgx = True

        for subKey in list(file_handle[tmpPath].keys()):
            if checkNxclass:
                try:
                    if file_handle[tmpPath][subKey].attrs['NX_class'
                    ].decode('utf-8') == nameClass:
                        if findRgx:
                            if bool(re.match(subPath, subKey)):
                                tmpPath = tmpPath + '/' + subKey
                        else:
                            tmpPath = tmpPath + '/' + subKey
                        if lvlPath == maxLvlPath - 1:
                            listFinalPath.append(tmpPath)
                            break
                except KeyError as e:
                    pass
                except AttributeError as at:
                    pass
            elif checkAttr:

                try:
                    match = bool(re.match(valueAttrToCheck,
                                          file_handle[tmpPath][subKey].attrs[nameAttrToCheck].decode('utf-8'
                                                                                                     )))
                    if match:
                        if findRgx:
                            if bool(re.match(subPath, subKey)):
                                tmpPath = tmpPath + '/' + subKey
                        else:
                            tmpPath = tmpPath + '/' + subKey

                        if lvlPath == maxLvlPath - 1:
                            listFinalPath.append(tmpPath)  # need to break for all dictionnaries except passerelle
                            break
                except KeyError as e:
                    pass
                except AttributeError as at:
                    pass
            else:

                if findRgx:
                    match = re.match(subPath, subKey)
                    if re.match(subPath, subKey):
                        if checkNxclass and not onlyNxsClass:
                            if file_handle[tmpPath][subKey].attrs['NX_class'
                            ].decode('utf-8') == nameClass:
                                tmpPath = tmpPath + '/' + subKey
                        if not checkNxclass:
                            tmpPath = tmpPath + '/' + subKey

                        if lvlPath == maxLvlPath - 1:
                            listFinalPath.append(tmpPath)
                else:
                    if subKey == subPath:
                        tmpPath = tmpPath + '/' + subKey
                        if lvlPath == maxLvlPath - 1:
                            listFinalPath.append(tmpPath)

    print('list Path is ')
    print(listFinalPath)
    return listFinalPath
