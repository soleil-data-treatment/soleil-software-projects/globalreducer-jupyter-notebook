import matplotlib.pyplot as plt
import numpy as np



def plotCurve(dataToPlot, nbImg):
    fig,axs = plt.subplots(nbImg)
    for indexPlot in range(0,nbImg):
        axs[indexPlot].plot(dataToPlot[indexPlot])
    
